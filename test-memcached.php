<?php 

$time_start = microtime(true);

$ac = new Memcached();
$ac->addServer('localhost', 11211);

#$ac->setOption(Memcached::OPT_SOCKET_SEND_SIZE, 1024*512);
#$ac->setOption(Memcached::OPT_SOCKET_RECV_SIZE, 1024*512);
$ac->setOption(Memcached::OPT_RECV_TIMEOUT, 5);
$ac->setOption(Memcached::OPT_SEND_TIMEOUT, 5);


$data = [];

for($i = 0; $i<30000; $i++){
  $data[] = md5($i);
}

$r = $ac->set('key', $data, 3600);

$set_time_end = microtime(true);

$set_time = $set_time_end - $time_start;

echo "Set Key Done in $set_time seconds \n";

$result = $ac->get('key');

$read_time_end = microtime(true);

$read_time = $read_time_end - $time_start;

echo "Read key Done in $read_time seconds \n";

$time_end = microtime(true);

$time = $time_end - $time_start;

echo "All Done in $time seconds\n";

?>
